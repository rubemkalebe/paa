#include <iostream>
#include <algorithm>
#include <cstdio>

using namespace std;

int max_subarray(int v[], int n) {
	int ans = 0, tmp = 0;
	for(int i = 0; i < n; i++) {
		tmp += v[i];
		ans = max(tmp, ans);
		if(tmp < 0) {
			tmp = 0;
		}
	}
	return ans;
}

int main() {

	int n;

	while(cin >> n && n != 0) {
		int v[n];
		for(int i = 0; i < n; i++) {
			cin >> v[i];
		}
		int max = max_subarray(v, n);
		if(max > 0) {
			printf("The maximum winning streak is %d.\n", max);
		} else {
			printf("Losing streak.\n");
		}
	}

	return 0;
}