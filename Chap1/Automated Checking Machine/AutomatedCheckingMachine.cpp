#include <iostream>
using namespace std;

int main() {

    int a[5], b[5];

LOOP:
    while(cin >> a[0]) {
        for(int i = 1; i < 5; i++) {
            cin >> a[i];
        }
        for(int i = 0; i < 5; i++) {
            cin >> b[i];
        }
        for(int i = 0; i < 5; i++) {
        	if(a[i] == b[i]) {
	            cout << "N" << endl;
	            goto LOOP; // Pula direto para a label LOOP
	        }
        }
        cout << "Y" << endl;
    }

    return 0;
}
