#include <iostream>
#include <cstdio>
using namespace std;

int main() {

	int m, n, count = 0;

	while(cin >> m >> n && m + n != 0) {
		char c1[m][n], c2[m][n];
		// Leitura da matriz
		for(int i = 0; i < m; i++) {
			for(int j = 0; j < n; j++) {
				cin >> c1[i][j];
			}
		}
		// Montagem da matriz com dicas
		for(int i = 0; i < m; i++) {
			for(int j = 0; j < n; j++) {
				if(c1[i][j] == '*') {
					c2[i][j] = '*';
				} else {
					int aux = 0;
					// Verifica diagonal superior esquerda
					if((i - 1 >= 0 && j - 1 >= 0) && c1[i-1][j-1] == '*') {
						aux++;
					}
					// Verifica acima
					if((i - 1 >= 0) && c1[i-1][j] == '*') {
						aux++;
					}
					// Verifica diagonal superior direita
					if((i - 1 >= 0 && j + 1 < n) && c1[i-1][j+1] == '*') {
						aux++;
					}
					// Verifica esquerda
					if((j - 1 >= 0) && c1[i][j-1] == '*') {
						aux++;
					}
					// Verifica direita
					if((j + 1 < n) && c1[i][j+1] == '*') {
						aux++;
					}
					// Verifica diagonal inferior esquerda
					if((i + 1 < m && j - 1 >= 0) && c1[i+1][j-1] == '*') {
						aux++;
					}
					// Verifica abaixo
					if((i + 1 < m) && c1[i+1][j] == '*') {
						aux++;
					}
					// Verifica diagonal inferior direita
					if((i + 1 < m && j + 1 < n) && c1[i+1][j+1] == '*') {
						aux++;
					}
					c2[i][j] = aux + '0';
				}
			}
		}
		// Impressao da matriz
		if(count > 0) cout << endl;
		printf("Field #%d:\n", ++count);
		for(int i = 0; i < m; i++) {
			int j;
			for(j = 0; j < n - 1; j++) {
				cout << c2[i][j];
			}
			cout << c2[i][j] << endl;
		}
	}

	return 0;
}