#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n;

	while(cin >> n && n != 0) {
		int v[n];
		for(int i = 0; i < n; i++) {
			cin >> v[i];
		}
		long long ans = 0;
		for(int i = 0; i < n - 1; i++) {
			ans += (abs(v[i]));
			v[i+1] += v[i];
		}
		cout << ans << endl;
	}

	return 0;
}