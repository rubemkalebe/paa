#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <iomanip>

#define MAX 100000
#define QUAD(x) ((x)*(x))

using namespace std;

struct Edge {
	int i, j;
	double k;

	bool operator<(const Edge &b) const {
		return k < b.k;
	}
};

struct Ponto {
	double x, y;
};

double dist(const Ponto &a, const Ponto &b) {
	return sqrt(QUAD(a.x - b.x) + QUAD(a.y - b.y));
}

Edge e[MAX+1];
int previous[MAX+1];

int parent(int a) {
	if(a == previous[a]) {
		return a;
	}
	return previous[a] = parent(previous[a]);
}

int main() {

	int t, n;

	cin >> t;

	while(t--) {
		cin >> n;
		vector<Ponto> pontos;
		
		for(int i = 0; i < n; i++) {
			Ponto p; cin >> p.x >> p.y;
			pontos.push_back(p);
		}

		int a = 0;

		for(int i = 0; i < n; i++) {
			for(int j = i + 1; j < n; j++) { // sempre considera pontos diferentes
				e[a].i = i;
				e[a].j = j;
				e[a].k = dist(pontos[i], pontos[j]);
				a++;
			}
		}

		sort(e, e+a);
		for(int i = 0; i < n; i++) previous[i] = i;

		double mst = 0.0;

		for(int i = 0; i < a; i++) {
			int u = parent(e[i].i);
			int v = parent(e[i].j);

			if(u != v) {
				mst += e[i].k;
				previous[u] = v;
			}
		}

		cout << fixed << setprecision(2) << mst << endl;
		if(t) cout << endl;
	}

	return 0;
}