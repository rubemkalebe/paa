#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;

struct Interval {
	double l, r;

	bool operator<(const Interval &b) const {
		return l < b.l;
	}
};

double range(double radius, int w) {
	return sqrt((radius*radius) - ((w / 2.0)*(w / 2.0)));
}

int main() {

	int n, l, w;

	while(cin >> n >> l >> w) {
		vector<Interval> sprinklers;
		for(int i = 0; i < n; i++) {
			Interval sprinkler;
			int pos, radius;
			cin >> pos >> radius;
			sprinkler.l = pos - range(radius, w);
			sprinkler.r = pos + range(radius, w);
			sprinklers.push_back(sprinkler);
		}

		sort(sprinklers.begin(), sprinklers.end());

		double largerCoverage = 0.0;
		int count = 0;

		for(int i = 0, j; i < n; i = j) {
			if(sprinklers[i].l > largerCoverage) {
				break;
			}
			for(j = i + 1; j < n && sprinklers[j].l <= largerCoverage; j++) {
				if(sprinklers[j].r > sprinklers[i].r) {
					i = j;
				}
			}
			count++;
			largerCoverage = sprinklers[i].r;
			if(largerCoverage >= l) {
				break;
			}
		}

		if(largerCoverage >= l) {
			cout << count << endl;
		} else {
			cout << "-1\n";
		}

	}

	return 0;
}