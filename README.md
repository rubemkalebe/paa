# Códigos e histórias #

### Apresentação ###

Este repositório contém todos os códigos-fonte desenvolvidos ao longo da disciplina de Projeto e Análise de Algoritmos (DCA0434), ministrada pelo Prof. Dr. Daniel Aloise, no semestre 2015.1.

O repositório está dividido em pastas referenciando os capítulos mostrados no relatório final. Cada pasta contém os problemas que foram propostos e cada problema se encontra em uma pasta interior.

Para mais informações consulte o *pdf* do relatório final.

### Sobre a linguagem de programação ###

Todos os programas presentes neste repositório foram implementados em linguagem **C++**.

### Como compilar? ###

Todos os códigos-fonte aqui expostos podem ser compilados, em sistemas *Linux-based*, através do seguinte comando:

    g++ nomeDoArquivo.cpp -o nomeDoExecutavel -std=c++11 -O2

IMPORTANTE: é necessário ter instalado o compilador g++.

### Equipe de desenvolvimento ###

* Rubem Kalebe (rubemkalebe@gmail.com)