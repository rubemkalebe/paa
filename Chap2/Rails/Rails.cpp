#include <iostream>
#include <stack>

using namespace std;

int main() {

	int n;

	while(cin >> n && n != 0) {
		int v[n];
		while(cin >> v[0]) {
			if(v[0] == 0) {
				cout << endl;
				break;
			} else {
				for(int i = 1; i < n; i++) {
					cin >> v[i];
				}
			}
			stack<int> s;
			int i = 0, j = 1;
			while(j <= n) {
				s.push(j);
				while(!s.empty() && s.top() == v[i]) {
					s.pop();
					i++;
					if(i >= n) break;
				}
				j++;
			}
			if(s.empty()) {
				cout << "Yes" << endl;
			} else {
				cout << "No" << endl;
			}
		}		
	}

	return 0;
}