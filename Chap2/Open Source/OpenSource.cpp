#include <iostream>
#include <algorithm>
#include <string>
#include <cctype>
#include <vector>
#include <map>

using namespace std;

struct Project {
	string name;
	int n;
};

bool comp(Project a, Project b) {
	if(a.n > b.n) return true;
	else if(a.n < b.n) return false;
	else return a.name < b.name;
} 

int main() {

	string str;
	Project p;
	vector<Project> projects;
	map<string, int> countStudents;
	int i;

	while(getline(cin, str) && str != "0") {
		if(str != "1") {
			if(isupper(str[0])) {
				// nome de projeto
				p.name = str;
				p.n = 0;
				i = projects.size();
				projects.push_back(p);
			} else {
				// userid
				pair < map <string, int>::iterator, bool > result = countStudents.insert(make_pair(str, i));
				if(result.second) { // se o elemento ainda nao existia
					projects[i].n++;
				} else if(result.first->second != i) {
					if(result.first->second != -1) {
						projects[result.first->second].n--;
						result.first->second = -1;
					}
				}
			}
		} else {
			sort(projects.begin(), projects.end(), comp);
			for(int i = 0; i < projects.size(); i++) {
				cout << projects[i].name << ' ' << projects[i].n << endl;
			}
			projects.clear();
			countStudents.clear();
		}
	}

	return 0;
}