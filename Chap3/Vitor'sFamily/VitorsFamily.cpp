#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

bool comp(int i, int j) {
	return i < j;
}

int main() {

	int n, r;

	while(cin >> n) {
		while(n--) {
			cin >> r;
			int v[r];
			for(int i = 0; i < r; i++) {
				cin >> v[i];
			}
			sort(v, v+r);
			int m = v[r/2], s = 0;
			for(int i = 0; i < r; i++) {
				s += abs(m - v[i]);
			}
			cout << s << endl;
		}
	}

	return 0;
}