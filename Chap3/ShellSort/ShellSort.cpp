#include <iostream>
#include <algorithm>
#include <vector>
#include <map>

using namespace std;

bool comp(pair<string, int> i, pair<string, int> j) {
	return i.second > j.second;
}

int main() {

	int c, n;
	string str;
	cin >> c;

	while(c--) {
		vector<string> v;
		map<string, int> wanted;
		vector< pair<string, int> > seq, order;
		cin >> n;
		cin.ignore();

		for(int i = 0; i < n; i++) {
			getline(cin, str);
			v.push_back(str);
		}

		for(int i = 0; i < n; i++) {
			getline(cin, str);
			wanted[str] = i;
		}

		for(int i = 0; i < n; i++) {
			seq.push_back(make_pair(v[i], wanted[v[i]]));
		}

		for(int i = n - 1, next = n - 1; i >= 0; i--) {
			if(seq[i].second != next) {
				order.push_back(seq[i]);
			} else {
				next--;
			}
		}
		sort(order.begin(), order.end(), comp);

		for(int i = 0; i < order.size(); i++) {
			cout << order[i].first << endl;
		}

		cout << endl;
	}

	return 0;
}