#include <iostream>
#include <vector>
#include <queue>
#include <set>

using namespace std;

class Graph {

public:
	int verticesNumber;
	vector< set<int> > listAdj;

	Graph(int n) {
		this->verticesNumber = n;
		this->listAdj.resize(this->verticesNumber);
	}

	void addEdge(int v, int w) {
		listAdj[v].insert(w);
	}

	int size() {
		return verticesNumber;
	}

};

bool isBipartite(Graph g) {
	// Roda uma BFS com um vetor de cores para verificar restricao
	vector<int> colors;
	colors.resize(g.size());
	queue<int> q;
	for(int i = 0; i < g.size(); i++) {
		colors[i] = -1; // Nenhuma cor, por enquanto
	}
	colors[0] = 0;
	q.push(0);
	while(!q.empty()) {
		int v = q.front();
		q.pop();
		// Procura vertices adjacentes
		for(auto it = g.listAdj[v].begin(); it != g.listAdj[v].end(); it++) {
			if(colors[*it] == -1) {
				colors[*it] = 1 - colors[v];
				q.push(*it);
			} else if(colors[*it] != 1 - colors[v]) {
				return false;
			}
		}
	}
	return true;
}

int main() {

	int n, l;

	while(cin >> n && n != 0) {
		cin >> l;
		Graph g(n);
		for(int i = 0; i < l; i++) {
			int v, w;
			cin >> v >> w;
			g.addEdge(v, w);
			g.addEdge(w, v);
		}
		if(isBipartite(g)) {
			cout << "BICOLORABLE." << endl;
		} else {
			cout << "NOT BICOLORABLE." << endl;
		}

	}

	return 0;
}