#include <iostream>
#include <cstdio>
#include <vector>
#include <queue>
#include <set>
#include <map>

using namespace std;

class Graph {

public:
	int verticesNumber;
	vector< vector<int> > listAdj;
	vector<int> inDegree;

	Graph(int n) {
		this->verticesNumber = n;
		this->listAdj.resize(this->verticesNumber);
		this->inDegree.resize(this->verticesNumber);
		inDegree.assign(this->verticesNumber, 0);
	}

	void addEdge(int v, int w) {
		listAdj[v].push_back(w);
		inDegree[w] += 1;
	}

	int size() {
		return verticesNumber;
	}

};

class Compare {

public:
    bool operator()(const int& a, const int& b) {
        return a > b;
    }

};

int main() {
	
	int n, m, count = 0;
	string s;

	while(cin >> n) {
		Graph g(n);
		map<string,int> name_value;
		map<int, string> value_name;
		priority_queue<int, vector<int>, Compare> q;
		vector<int> order;
		for(int i = 0; i < n; i++) {
			cin >> s;
			name_value.insert(pair<string, int>(s, i));
			value_name.insert(pair<int, string>(i, s));
		}
		cin >> m;
		for(int i = 0; i < m; i++) {
			string start, end;
			cin >> start >> end;
			int v = name_value.find(start)->second;
			int w = name_value.find(end)->second;
			g.addEdge(v, w);
		}
		for (int i = 0; i < n; ++i) {
			if(g.inDegree[i] == 0) q.push(i);
		}
		while(!q.empty()) {
			int v = q.top();
			q.pop();
			order.push_back(v);
			for(auto it = g.listAdj[v].begin(); it != g.listAdj[v].end(); it++) {
				g.inDegree[*it] -= 1;
				if(g.inDegree[*it] == 0) {
					q.push(*it);
				}
			}
		}
		printf("Case #%d: Dilbert should drink beverages in this order: %s",++count, value_name[order[0]].data());
        for(int i = 1; i < order.size(); i++) {
            printf(" %s",  value_name[order[i]].data());
        }
        printf(".\n\n");
	}

	return 0;
}