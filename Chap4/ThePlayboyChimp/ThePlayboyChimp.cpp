#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

// SOLUÇÃO O(n)

int main() {

    int n, q, index, lucho;

    while(cin >> n) {
        int ladies[n];
        for(int i = 0; i < n; i++) {
            cin >> ladies[i];
        }
        cin >> q;
        for(int i = 0; i < q; i++) {
            cin >> lucho;
            // Procura por maior menor
            int maiorMenor = 0;
            for(int j = 0; j < n; j++) {
                if(lucho > ladies[j]) {
                    maiorMenor = ladies[j];
                } else {
                    break;
                }
            }
            if(maiorMenor == 0) {
                cout << "X ";
            } else {
                cout << maiorMenor << ' ';
            }
            // Procura por menor maior
            int menorMaior = 0;
            for(int j = n - 1; j >= 0; j--) {
                if(ladies[j] > lucho) {
                    menorMaior = ladies[j];
                } else {
                    break;
                }
            }
            if(menorMaior == 0) {
                cout << "X\n";
            } else {
                cout << menorMaior << endl;
            }
        }
    }

    return 0;
}

/* SOLUÇÃO O(log n)

int buscaBinaria(int v[], int inf, int sup, int x, int &possivelIndice){
    if(inf > sup){
        possivelIndice = ceil((inf + sup) / 2.0);
        return -1;
    }
    int meio = (inf + sup) / 2;
    if(v[meio] == x) {
        return meio;
    } else if(x < v[meio]) {
        return buscaBinaria(v, inf, meio - 1, x, possivelIndice);
    } else {
        return buscaBinaria(v, meio + 1, sup, x, possivelIndice);
    }
}

int main() {

    int n, q, index, lucho, possivelIndice;

    scanf("%d", &n);

    int ladies[n];

    for(int i = 0; i < n; i++) {
        scanf("%d", &ladies[i]);
    }
    
    scanf("%d", &q);

    for(int i = 0; i < q; i++) {
        scanf("%d", &lucho);
        // Procura por maior menor
        index = buscaBinaria(ladies, 0, n-1, lucho - 1, possivelIndice);
        if(index != -1) {
            printf("%d ", ladies[index]);
        } else if(possivelIndice - 1 >= 0) {
            printf("%d ", ladies[possivelIndice - 1]);
        } else {
            printf("X ");
        }
        // Procura por menor maior
        index = buscaBinaria(ladies, 0, n-1, lucho + 1, possivelIndice);
        if(index != -1) {
            printf("%d\n", ladies[index]);
        } else if(possivelIndice + 1 < n) {
            printf("%d\n", ladies[possivelIndice + 1]);
        } else {
            printf("X\n");
        }
    }

    return 0;
}

int buscaBinaria(int v[], int inf, int sup, int x, int &possivelIndice){
    if(inf > sup){
        possivelIndice = ceil((inf + sup) / 2.0);
        return -1;
    }
    int meio = (inf + sup) / 2;
    if(v[meio] == x) {
        return meio;
    } else if(x < v[meio]) {
        return buscaBinaria(v, inf, meio - 1, x, possivelIndice);
    } else {
        return buscaBinaria(v, meio + 1, sup, x, possivelIndice);
    }
}

int main() {

    int n, q, index, lucho, possivelIndice;

    scanf("%d", &n);

    int ladies[n];

    for(int i = 0; i < n; i++) {
        scanf("%d", &ladies[i]);
    }
    
    scanf("%d", &q);

    int lucho[q];

    for(int i = 0; i < q; i++) {
        scanf("%d", &lucho[i]);
    }

    for(int i = 0; i < q; i++) {
        // Procura por maior menor
        index = buscaBinaria(ladies, 0, n-1, lucho[i] - 1, possivelIndice);
        if(index != -1) {
            printf("%d ", ladies[index]);
        } else if(possivelIndice - 1 >= 0) {
            printf("%d ", ladies[possivelIndice - 1]);
        } else {
            printf("X ");
        }
        // Procura por menor maior
        index = buscaBinaria(ladies, 0, n-1, lucho[i] + 1, possivelIndice);
        if(index != -1) {
            printf("%d\n", ladies[index]);
        } else if(possivelIndice + 1 < n) {
            printf("%d\n", ladies[possivelIndice + 1]);
        } else {
            printf("X\n");
        }
    }

    return 0;
}
*/