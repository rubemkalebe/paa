#include <iostream>
#include <cmath>

using namespace std;

int search(int node, int balls, int max) {
	int left = node * 2, right = left + 1;
	if(left < max && right < max) {
		if(balls % 2 == 0) {
			search(right, balls / 2, max);
		} else {
			search(left, (balls / 2) + 1, max);
		}
	} else {
		return node;
	}
}

int main() {

	int n, d, i;

	while(cin >> n && n != -1) {
		while(n--) {
			cin >> d >> i;
			cout << search(1, i, pow(2, d)) << endl;
		}
	}

	return 0;
}