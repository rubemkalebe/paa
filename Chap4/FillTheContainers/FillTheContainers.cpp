#include <iostream>

using namespace std;

bool canFill(int v[], int m, int max, int n) {
	int sum = 0;
	for(int i = 0; i < n; i++) {
		if(sum + v[i] <= max) {
			sum += v[i];
		} else {
			m--;
			sum = v[i];
		}
	}
	return sum <= max && m > 0;
}

int main() {

	int n, m;

	while(cin >> n >> m) {
		int v[n];
		int min = 0, max = 0;
		for(int i = 0; i < n; i++) {
			cin >> v[i];
			if(v[i] > min) {
				min = v[i];
			}
			max += v[i];
		}
		int maxCapacity = max;
		while(max > min) {
			int mid = (min + max) / 2;
			if(canFill(v, m, mid, n)) {
				maxCapacity = mid;
				max = mid;
			} else {
				min = mid + 1;
			}
		}
		cout << maxCapacity << endl;
	}

	return 0;
}