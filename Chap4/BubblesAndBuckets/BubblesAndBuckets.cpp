#include <iostream>

using namespace std;

typedef unsigned long long ull;

ull swaps;
 
void Merge(ull a[], ull temp[], ull low, ull high) {
	ull mid = (high + low) / 2;
	ull i = low;
	ull j = mid + 1;
	ull k = low;

    while(i <= mid && j <= high) {
        if(a[i] <= a[j]) {
            temp[k++] = a[i++];
        } else {
            temp[k++] = a[j++];
            swaps += (mid - i + 1); // numero de posicoes que 'pulou'
        }
    }

    while(i <= mid) {
    	temp[k++] = a[i++];
    }

    while(j <= high) {
    	temp[k++] = a[j++];
    }

	for(ull z = low; z <= high; z++) {
		a[z] = temp[z];
	}
}
 
void MergeSort(ull a[], ull temp[], ull low, ull high) {
    if(low < high) {
        ull mid = (low + high) / 2;
        MergeSort(a, temp, low, mid);
        MergeSort(a, temp, mid + 1, high);
        Merge(a, temp, low, high);
    }
}

int main() {

    ull n;
    
    while(cin >> n && n != 0) {
		ull a[n], t[n];
		for(int i = 1; i <= n; i++) {
			cin >> a[i];
		}
		swaps = 0;
		MergeSort(a, t, 1, n);
        if(swaps % 2 != 0) {
            cout << "Marcelo" << endl;
        } else {
            cout << "Carlos" << endl;
        }		
     }
 
    return 0;
}