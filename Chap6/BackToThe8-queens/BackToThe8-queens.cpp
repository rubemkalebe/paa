#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

bool firstSolution;
int rowPosition[8];
int tmp[8];
int minMoves;

bool isValid(int i, int col) {
	for(int j = 0; j < col; j++) {
		if((i == tmp[j]) || (abs(i - tmp[j]) == abs(col - j))) {
			return false;
		}
	}
	return true;
}

void findBestSolution(int row) {
	for(int i = 0; i < 8; i++) {
		if(isValid(i, row)) {
			tmp[row] = i;
			if(row == 7) {
				int newMinMoves = 0;
				for(int j = 0; j < 8; j++) {
					newMinMoves += (rowPosition[j] != tmp[j]);
				}
				if(firstSolution || newMinMoves < minMoves) {
					firstSolution = false;
					minMoves = newMinMoves;
				}
			} else {
				findBestSolution(row + 1);
			}
		}
	}
}

int main() {

	int count = 0;

	while(cin >> rowPosition[0]) {
		rowPosition[0] -= 1;
		for(int i = 1; i < 8; i++) {
			cin >> rowPosition[i];
			rowPosition[i] -= 1;
		}
		firstSolution = true;
		findBestSolution(0);
		printf("Case %d: %d\n", ++count, minMoves);
	}

	return 0;
}