#include <iostream>

using namespace std;

int main() {

	int n, numberOfTracks, sum, currentSum, bestIndex, tracks[21];
	
	while(cin >> n) {
		cin >> numberOfTracks;
		
		for(int i = 0; i < numberOfTracks; i++) {
			cin >> tracks[i];
		}
		
		int max = 1 << numberOfTracks; // qntd maxima de subconjuntos
		sum = bestIndex = 0;
		
		for(int i = 0; i < max; i++) {
		
			currentSum = 0;
		
			for(int j = 0; j < numberOfTracks; j++) {
				if((1 << j) & i) {
					currentSum += tracks[j];
				}
			}
		
			if(currentSum > sum && currentSum <= n) {
				bestIndex = i;
				sum = currentSum;
			}
		}
		
		for(int i = 0; i < numberOfTracks; i++) {
			if((1 << i) & bestIndex) {
				cout << tracks[i] << ' ';
			}
		}
		cout << "sum:" << sum << endl;
	}


	return 0;
}